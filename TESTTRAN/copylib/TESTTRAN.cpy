       01  TSTTRNAI.
           02  FILLER PIC X(12).
           02  TXTREADL  COMP PIC S9(4).
           02  TXTREADF  PICTURE X.
           02  FILLER REDEFINES TXTREADF.
               03  TXTREADA  PICTURE X.
           02  TXTREADC  PICTURE X.
           02  TXTREADP  PICTURE X.
           02  TXTREADH  PICTURE X.
           02  TXTREADV  PICTURE X.
           02  TXTREADI  PIC X(9).
           02  TXTUPDTL  COMP PIC S9(4).
           02  TXTUPDTF  PICTURE X.
           02  FILLER REDEFINES TXTUPDTF.
               03  TXTUPDTA  PICTURE X.
           02  TXTUPDTC  PICTURE X.
           02  TXTUPDTP  PICTURE X.
           02  TXTUPDTH  PICTURE X.
           02  TXTUPDTV  PICTURE X.
           02  TXTUPDTI  PIC X(9).
           02  TXTCTRLL  COMP PIC S9(4).
           02  TXTCTRLF  PICTURE X.
           02  FILLER REDEFINES TXTCTRLF.
               03  TXTCTRLA  PICTURE X.
           02  TXTCTRLC  PICTURE X.
           02  TXTCTRLP  PICTURE X.
           02  TXTCTRLH  PICTURE X.
           02  TXTCTRLV  PICTURE X.
           02  TXTCTRLI  PIC X(9).
           02  TXTALTRL  COMP PIC S9(4).
           02  TXTALTRF  PICTURE X.
           02  FILLER REDEFINES TXTALTRF.
               03  TXTALTRA  PICTURE X.
           02  TXTALTRC  PICTURE X.
           02  TXTALTRP  PICTURE X.
           02  TXTALTRH  PICTURE X.
           02  TXTALTRV  PICTURE X.
           02  TXTALTRI  PIC X(9).
           02  CANREADL  COMP PIC S9(4).
           02  CANREADF  PICTURE X.
           02  FILLER REDEFINES CANREADF.
               03  CANREADA  PICTURE X.
           02  CANREADC  PICTURE X.
           02  CANREADP  PICTURE X.
           02  CANREADH  PICTURE X.
           02  CANREADV  PICTURE X.
           02  CANREADI  PIC X(39).
           02  CANUPDTL  COMP PIC S9(4).
           02  CANUPDTF  PICTURE X.
           02  FILLER REDEFINES CANUPDTF.
               03  CANUPDTA  PICTURE X.
           02  CANUPDTC  PICTURE X.
           02  CANUPDTP  PICTURE X.
           02  CANUPDTH  PICTURE X.
           02  CANUPDTV  PICTURE X.
           02  CANUPDTI  PIC X(39).
           02  CANCTRLL  COMP PIC S9(4).
           02  CANCTRLF  PICTURE X.
           02  FILLER REDEFINES CANCTRLF.
               03  CANCTRLA  PICTURE X.
           02  CANCTRLC  PICTURE X.
           02  CANCTRLP  PICTURE X.
           02  CANCTRLH  PICTURE X.
           02  CANCTRLV  PICTURE X.
           02  CANCTRLI  PIC X(39).
           02  CANALTRL  COMP PIC S9(4).
           02  CANALTRF  PICTURE X.
           02  FILLER REDEFINES CANALTRF.
               03  CANALTRA  PICTURE X.
           02  CANALTRC  PICTURE X.
           02  CANALTRP  PICTURE X.
           02  CANALTRH  PICTURE X.
           02  CANALTRV  PICTURE X.
           02  CANALTRI  PIC X(39).
           02  THETRNL  COMP PIC S9(4).
           02  THETRNF  PICTURE X.
           02  FILLER REDEFINES THETRNF.
               03  THETRNA  PICTURE X.
           02  THETRNC  PICTURE X.
           02  THETRNP  PICTURE X.
           02  THETRNH  PICTURE X.
           02  THETRNV  PICTURE X.
           02  THETRNI  PIC X(4).
           02  TXTMSGL  COMP PIC S9(4).
           02  TXTMSGF  PICTURE X.
           02  FILLER REDEFINES TXTMSGF.
               03  TXTMSGA  PICTURE X.
           02  TXTMSGC  PICTURE X.
           02  TXTMSGP  PICTURE X.
           02  TXTMSGH  PICTURE X.
           02  TXTMSGV  PICTURE X.
           02  TXTMSGI  PIC X(40).
           02  INFMSGL  COMP PIC S9(4).
           02  INFMSGF  PICTURE X.
           02  FILLER REDEFINES INFMSGF.
               03  INFMSGA  PICTURE X.
           02  INFMSGC  PICTURE X.
           02  INFMSGP  PICTURE X.
           02  INFMSGH  PICTURE X.
           02  INFMSGV  PICTURE X.
           02  INFMSGI  PIC X(79).
       01  TSTTRNAO REDEFINES TSTTRNAI.
           02  FILLER PIC X(12).
           02  FILLER PICTURE X(7).
           02  TXTREADO  PIC X(9).
           02  FILLER PICTURE X(7).
           02  TXTUPDTO  PIC X(9).
           02  FILLER PICTURE X(7).
           02  TXTCTRLO  PIC X(9).
           02  FILLER PICTURE X(7).
           02  TXTALTRO  PIC X(9).
           02  FILLER PICTURE X(7).
           02  CANREADO  PIC X(39).
           02  FILLER PICTURE X(7).
           02  CANUPDTO  PIC X(39).
           02  FILLER PICTURE X(7).
           02  CANCTRLO  PIC X(39).
           02  FILLER PICTURE X(7).
           02  CANALTRO  PIC X(39).
           02  FILLER PICTURE X(7).
           02  THETRNO  PIC X(4).
           02  FILLER PICTURE X(7).
           02  TXTMSGO  PIC X(40).
           02  FILLER PICTURE X(7).
           02  INFMSGO  PIC X(79).
