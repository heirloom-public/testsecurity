var socket;
var new_uri;
var termId;
var contextRoot;
var replContextRoot;
//$(document).ready(init);

isLoading=false;
isloadingOverlay();

function init() {
    isloadingOverlay();
    var loc = window.location;
    var pathArray = loc.pathname.split( '/' );
    
    if (loc.protocol === "https:") {
        new_uri = "wss:";
    } else {
        new_uri = "ws:";
    }
    new_uri += "//" + loc.host;
    contextRoot = "";
    if (pathArray[1] !== "servlet") {    
        contextRoot = pathArray[1];
        new_uri += "/" + contextRoot;
    }
    new_uri += "/terminalwebsocket";
    
    if (contextRoot !== "") {
        replContextRoot = "/" + contextRoot;
    } else {
        replContextRoot = "";
    }
    
    connect();
}

function connect(){
    try{
        socket = new WebSocket(new_uri);

       socket.onopen = function(){
           socket.send(termId);
       }

       socket.onmessage = function(event){
           socket.close();
           // similar behavior as an HTTP redirect
           window.location.replace(replContextRoot + event.data);
       }
       socket.onclose = function(){
               //message('<p class="event">Socket Status: '+socket.readyState+' (Closed)');
       }            

   } catch(exception){
       console.log(exception);
   }
}